import electron, { app, BrowserWindow, screen } from 'electron';
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';
import { enableLiveReload } from 'electron-compile';
import Store from 'electron-store';
import { isUndefined } from 'util';
const uuid = require("uuid/v5")

const store = new Store();

const isDevMode = process.execPath.match(/[\\/]electron/);


// Start app on login
if(!isDevMode){
  const appFolder = path.dirname(process.execPath)
  const updateExe = path.resolve(appFolder, '..', 'Update.exe')
  const exeName = path.basename(process.execPath)
  
  app.setLoginItemSettings({
    openAtLogin: true,
    path: process.execPath,
    args: [
      '--processStart', `"${exeName}"`,
      '--process-start-args', `"--hidden"`
    ]
  })

}


var displayConfigs = [
    {url:"http://192.168.1.44:8080/#/macro/pupitre/pupitregauche"},
];

let machineId;
if(isDevMode){
  machineId = "dev";
}
if(1){
  machineId = store.get("machineId");
  if(isUndefined(machineId)){
    machineId = uuid("display-manager.ponymakers.com", uuid.DNS)
    machineId = store.set("machineId", machineId);
  }
}

enableLiveReload();

const createWindow = async () => {
 
  await installExtension(VUEJS_DEVTOOLS);
  updateDisplays();
};

app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

////////////////
//  DISPLAYS  //
////////////////

var displayWindows = [];
const updateDisplays = async () => {

  const displays = screen.getAllDisplays()
  console.log(displays)

  for(let window of displayWindows){
    try{
      window.close();
    }catch(err){
      //console.log(err);
    }
  };
  displayWindows = [];
  for(let i = 0; i<displays.length; i++){
    const display = displays[i];
    const { x, y, width, height} = display.workArea;
    const displayId = i;
    const displayConfig = displayConfigs[i];


    let window = new BrowserWindow({
        width, height,
        x, y,
        show:false, 
        webPreferences: {
          nodeIntegration: isUndefined(displayConfig),
        }
    })

    window.on('closed', () => {

    })

  
    if(displayConfig && displayConfig.url){
      let url = displayConfig.url;
      window.loadURL(url);
      console.log("show", displayId, url);
      window.once('ready-to-show', () => {
          window.show();
          window.setFullScreen(true);
      })
      displayWindows.push(window);
    }


  }
}
